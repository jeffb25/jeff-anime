package com.rave.feature_browse.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.rave.feature_browse.data.local.dao.AnimeDao
import com.rave.feature_browse.data.local.entity.Anime
import dagger.Provides


/**
 * Create and setup DB
 */
@Database(entities = [Anime::class], version = 1, exportSchema = false)
abstract class JikanDatabase : RoomDatabase() {

    abstract fun getAnimeDao(): AnimeDao

}