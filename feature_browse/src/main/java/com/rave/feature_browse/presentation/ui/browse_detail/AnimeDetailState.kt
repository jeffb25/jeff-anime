package com.rave.feature_browse.presentation.ui.browse_detail

import com.rave.feature_browse.data.local.entity.Anime

data class AnimeDetailState(
    val animeList: Anime = Anime(title = "Title", imageUrl = "Image", synopsis = "no info"),
    val errorMsg: String? = null,
    val isLoading: Boolean = false
    )