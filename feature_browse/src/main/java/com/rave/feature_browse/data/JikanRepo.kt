package com.rave.feature_browse.data

import com.rave.feature_browse.data.local.JikanDatabase
import com.rave.feature_browse.data.local.dao.AnimeDao
import com.rave.feature_browse.data.local.entity.Anime
import com.rave.feature_browse.data.remote.JikanService
import com.rave.feature_browse.data.remote.response.AnimeResponseDTO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * How many params does the constructor have?
 * 2 params
 * Why do we add "@Inject constructor" around our constructor
 * statically defining the list of required dependencies by specifying them as parameters to the class's constructor
 * Why do we use suspend keyword?
 * to pause and resume when needed
 * Why do we use withContext below and how many params does it take?
 * different way to writing async, when used runs in series not parallel
 * Why are we passing in Dispatchers.IO in the withContext?
 * to signal which other thread we want to run this on
 */
class JikanRepo @Inject constructor(
    private val jikanService: JikanService,
    jikanDatabase: JikanDatabase
) {
    private val animeDao = jikanDatabase.getAnimeDao()

    suspend fun getAnimeList() = withContext(Dispatchers.IO) {
        val cashedAnime: List<Anime> = animeDao.getAnime()

        return@withContext cashedAnime.ifEmpty {
            val animeData: AnimeResponseDTO = jikanService.getAnimeList()
            val anime: List<Anime> = animeData.animeList.map {
                Anime(title = it.title, imageUrl = it.images.jpg.largeImageUrl, synopsis = it.synopsis)
            }

            animeDao.insert(anime)
            return@ifEmpty anime

        }
    }

    suspend fun getAnimeById(id: Int): Anime {
        return animeDao.getAnimeById(id)

    }

}