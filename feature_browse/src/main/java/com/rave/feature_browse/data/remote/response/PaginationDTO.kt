package com.rave.feature_browse.data.remote.response

import com.google.gson.annotations.SerializedName

data class PaginationDTO(
    @SerializedName("has_next_page")
    val hasNextPage: Boolean,
    @SerializedName("last_visible_page")
    val lastVisiblePage: Int
)