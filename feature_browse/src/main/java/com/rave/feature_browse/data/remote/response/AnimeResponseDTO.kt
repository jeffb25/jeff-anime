package com.rave.feature_browse.data.remote.response

import com.google.gson.annotations.SerializedName

data class AnimeResponseDTO(
    @SerializedName("data")
    val animeList: List<AnimeDTO>,
    val pagination: PaginationDTO
)