package com.rave.feature_browse.data.remote.response

import com.google.gson.annotations.SerializedName

data class TrailerDTO(
    @SerializedName("embed_url")
    val embedUrl: String?,
    @SerializedName("images")
    val trailerImage: TrailerImageDTO,
    val url: String?,
    @SerializedName("youtube_id")
    val youtubeId: String?
)