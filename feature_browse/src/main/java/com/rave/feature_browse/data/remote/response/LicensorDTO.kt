package com.rave.feature_browse.data.remote.response

import com.google.gson.annotations.SerializedName

data class LicensorDTO(
    @SerializedName("mal_id")
    val malId: Int,
    val name: String,
    val type: String,
    val url: String
)