package com.rave.feature_browse.presentation.viewmodel

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.rave.feature_browse.data.local.entity.Anime
import com.rave.feature_browse.domain.GetAnimeUseCase
import com.rave.feature_browse.presentation.ui.browse.AnimeListState
import com.rave.feature_browse.presentation.ui.browse_detail.AnimeDetailState
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

/**
 * Update this viewmodel to
 * - take a usecase that lets user get Anime update based on the animeId
 * - update the view with the state after it fetches the Anime object
 */
class AnimeDetailViewModel @AssistedInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    getAnimeUseCase: GetAnimeUseCase
) : ViewModel() {

    private val animeId = savedStateHandle.get<Int>("animeId")!!

    val browseAnimeState = liveData {
        emit(AnimeDetailState(isLoading = true))
        val animeResult: Result<Anime> = getAnimeUseCase(animeId)
        val state = AnimeDetailState(
            animeList = animeResult.getOrNull() ?: Anime(), // explain this line
            errorMsg = animeResult.exceptionOrNull()?.message // explain this line
        )
        emit(state)
    }



    @AssistedFactory
    interface Factory {
        fun create(handle: SavedStateHandle): AnimeDetailViewModel
    }
}