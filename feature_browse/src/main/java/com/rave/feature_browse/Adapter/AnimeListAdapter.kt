package com.rave.feature_browse.Adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rave.feature_browse.data.local.entity.Anime
import com.rave.feature_browse.databinding.ItemAnimeBinding
import com.rave.feature_browse.presentation.ui.browse.AnimeListFragmentDirections
import com.squareup.picasso.Picasso

class AnimeListAdapter() : RecyclerView.Adapter<AnimeListAdapter.AnimeListViewHolder>() {

    var animeList = mutableListOf<Anime>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) = AnimeListViewHolder.getInstance(parent).apply {
        itemView.setOnClickListener { animeList }
    }


    override fun getItemCount(): Int {
        return animeList.size
    }

    override fun onBindViewHolder(holder: AnimeListViewHolder, position: Int) {
        holder.addAnimeCard(animeList[position])


    }

    fun addAnimeList(animeList: List<Anime>) {
        this.animeList = animeList.toMutableList()
        notifyDataSetChanged()
    }

    class AnimeListViewHolder(private val binding: ItemAnimeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun addAnimeCard(anime: Anime) {
            binding.run {
//                ivAnimeImage.load(anime.imageUrl)
                tvAnimeTitle.text = anime.title
                Picasso.get().load(anime.imageUrl).into(ivAnimeImage)
                ivAnimeImage.setOnClickListener {
                    it.findNavController().navigate(AnimeListFragmentDirections.actionToAnimeDetailFragment(anime.id)
                    )
                }
            }
        }


        companion object {
            fun getInstance(
                parent: ViewGroup
            ) = ItemAnimeBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ).let { binding -> AnimeListViewHolder(binding) }

        }

    }
}