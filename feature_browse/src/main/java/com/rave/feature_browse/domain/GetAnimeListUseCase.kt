package com.rave.feature_browse.domain

import com.rave.feature_browse.data.JikanRepo
import com.rave.feature_browse.data.local.entity.Anime
import com.rave.jikan.annotations.DefaultDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * Refer to the video below to learn more about the DOMAIN layer and UseCases
 * https://www.youtube.com/watch?v=gIhjCh3U88I
 *
 * Once you watched video go through you'll be a better understanding of this UseCase concept
 */
class GetAnimeListUseCase @Inject constructor(
    private val jikanRepo: JikanRepo,
    @DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
) {

    /**
     * suspend - this makes this function a coroutine function so we can do this work on a differnt thread
     * operator fun invoke() - this is a little code sugar (look at how this usecase is used in the viewmodel to understand better)
     * Result<List<Anime>> -> Result is just a container class with special properties that will hold List<Anime>. It's special properties lets us
     *                          attach a status of success or failure
     *
     */
    suspend operator fun invoke(): Result<List<Anime>> = withContext(defaultDispatcher) {

        return@withContext try {
            // First we fetch from the server, if this fails and throws an error we will jump to the "catch" scope
            val animeResponse = jikanRepo.getAnimeList()
            // If we make it here means no errors were thrown while fetching from server and we got a list back
            // So we will convert the AnimeDTO to Anime(in our entity package)
            val animeList = animeResponse
            // Return the anime list in the result container and mark it as success by using the method
            Result.success(animeList)
        } catch (ex: Exception) {
            // Return the error in the result container and mark it as failure by using the method
            Result.failure(ex)
        }
    }
}