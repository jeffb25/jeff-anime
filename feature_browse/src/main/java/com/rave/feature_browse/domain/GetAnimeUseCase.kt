package com.rave.feature_browse.domain

import com.rave.feature_browse.data.JikanRepo
import com.rave.feature_browse.data.local.entity.Anime
import com.rave.jikan.annotations.DefaultDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * Create a UseCase that returns a single anime item given the id is provided
 */
class GetAnimeUseCase @Inject constructor(
    private val repo: JikanRepo,
    @DefaultDispatcher private val dispatcher: CoroutineDispatcher
){
    suspend operator fun invoke(id: Int): Result<Anime> = withContext(dispatcher) {
        try {
            val animeRespose = repo.getAnimeById(id)

            val animelist = animeRespose

             Result.success(animelist)
        } catch (ex: Exception) {
            Result.Companion.failure(ex)
        }
    }
}