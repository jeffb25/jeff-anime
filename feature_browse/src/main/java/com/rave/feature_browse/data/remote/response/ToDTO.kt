package com.rave.feature_browse.data.remote.response

data class ToDTO(
    val day: Int?,
    val month: Int?,
    val year: Int?
)