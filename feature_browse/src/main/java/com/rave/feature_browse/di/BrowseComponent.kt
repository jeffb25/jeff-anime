package com.rave.feature_browse.di

import android.content.Context
import com.rave.feature_browse.presentation.ui.browse.AnimeListFragment
import com.rave.feature_browse.presentation.ui.browse_detail.AnimeDetailFragment
import com.rave.jikan.di.DispatcherDependencies
import dagger.BindsInstance
import dagger.Component

/**
 * DI = Dependency Injection
 * THIS IS USED IN THE [AnimeListFragment]
 *
 * This interface allows me to use DI in my feature module, Hilt is built on top of dagger so in
 * our feature modules we need to use the default dagger implementation rather than the Hilt annotations
 *
 * The @Component lets us add
 *      modules - which is the Instances we want to inject within our feature module
 *      dependencies - These are instances that come from the "app(phone/tablet) module" its how we
 *                      use them within our feature module
 *
 *
 */
@Component(
    modules = [RemoteModule::class, LocalModule::class],
    dependencies = [DispatcherDependencies::class]
)
interface BrowseComponent {

    /**
     * We define inject method for all the Views/Destinations we plan to inject instances into
     *
     * so far we only have one fragment defined but we can add as many as we want (similar to DAO process in Database Implementation)
     */
    fun inject(browseAnimeFragment: AnimeListFragment)
    fun inject(animeDetailFragment: AnimeDetailFragment)

    /**
     * Below we define methods to add dependencies coming from app and context which might be need
     */
    @Component.Builder
    interface Builder {
        fun context(@BindsInstance context: Context): Builder
        fun dispatcherDependencies(dispatcherDependencies: DispatcherDependencies): Builder
        fun build(): BrowseComponent
    }
}