package com.rave.feature_browse.presentation.ui.browse

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.rave.feature_browse.Adapter.AnimeListAdapter
import com.rave.feature_browse.databinding.FragmentAnimeListBinding
import com.rave.feature_browse.di.DaggerBrowseComponent
import com.rave.feature_browse.presentation.viewmodel.AnimeListViewModel
import com.rave.jikan.di.DispatcherDependencies
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.EntryPointAccessors
import javax.inject.Inject

/**
 * What is a Fragment?
 * How many lifecycles does an fragment have?
 * What happens in each lifecycle?
 */

class AnimeListFragment : Fragment() {

    private var _binding: FragmentAnimeListBinding? = null
    private val binding get() = _binding!!
    private val animeListAdapter by lazy { AnimeListAdapter() }

    @Inject
    lateinit var animeListViewModel: AnimeListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        initDagger()
        super.onCreate(savedInstanceState)
    }

    /**
     * How many params does onCreateView have?
     * 3
     * What is the return type of onCreateView?
     * object
     * What is [FragmentAnimeListBinding]?
     * binding class created for anime list fragment
     * What kind of method is inflate and how many params does it take if any?
     * adds view to activity during run time
     * What special kind of kotlin function are we using below and why are we using it?
     * onViewCreated
     * What is the last thing we do with the instance of [FragmentAnimeListBinding] and why?
     * binding the onDestroyView so it clears view when it is destroyed
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentAnimeListBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /**
         * Explain what we are doing with the browseAnimeViewModel in the line below
         * adds observer to existing lifecyclen life span
         */
        animeListViewModel.browseAnimeState.observe(viewLifecycleOwner) { state ->
            Log.d("BrowseAnimeFragment", state.toString())
            binding.rvAnimeList.layoutManager = GridLayoutManager(this.context, 2)
            binding.rvAnimeList.adapter = animeListAdapter.apply {
                addAnimeList(state.animeList)
            }
            /**
             * Using State
             * if error != null -> display error dialog with error message
             * if isLoading -> display CircularProgressIndicator
             * update the Recyclerview you create
             */
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initDagger() {
        val appDependencies = EntryPointAccessors.fromApplication(
            requireContext().applicationContext, DispatcherDependencies::class.java
        )
        DaggerBrowseComponent.builder()
            .context(requireContext())
            .dispatcherDependencies(appDependencies)
            .build()
            .inject(this)
    }


}