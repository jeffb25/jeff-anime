package com.rave.feature_browse.data.remote.response

import com.google.gson.annotations.SerializedName

data class AnimeDTO(
    val aired: AiredDTO,
    val airing: Boolean,
    val background: String?,
    val broadcast: BroadcastDTO,
    val demographics: List<DemographicDTO>,
    val duration: String,
    val episodes: Int?,
    @SerializedName("explicit_genres")
    val explicitGenres: List<Any>,
    val favorites: Int,
    val genres: List<GenreDTO>,
    val images: ImagesDTO,
    val licensors: List<LicensorDTO>,
    @SerializedName("mal_id")
    val malId: Int,
    val members: Int,
    val popularity: Int,
    val producers: List<ProducerDTO>,
    val rank: Int,
    val rating: String,
    val score: Double,
    @SerializedName("scored_by")
    val scoredBy: Int,
    val season: String?,
    val source: String,
    val status: String,
    val studios: List<StudioDTO>,
    val synopsis: String,
    val themes: List<ThemeDTO>,
    val title: String,
    @SerializedName("title_english")
    val titleEnglish: String?,
    @SerializedName("title_japanese")
    val titleJapanese: String,
    @SerializedName("title_synonyms")
    val titleSynonyms: List<String>,
    val trailer: TrailerDTO,
    val type: String,
    val url: String,
    val year: Int?
)