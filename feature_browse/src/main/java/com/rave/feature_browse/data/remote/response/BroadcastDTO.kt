package com.rave.feature_browse.data.remote.response

data class BroadcastDTO(
    val day: String?,
    val string: String?,
    val time: String?,
    val timezone: String?
)