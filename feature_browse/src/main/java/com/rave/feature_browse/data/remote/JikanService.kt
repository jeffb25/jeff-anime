package com.rave.feature_browse.data.remote

import com.rave.feature_browse.data.local.dao.AnimeDao
import com.rave.feature_browse.data.local.entity.Anime
import com.rave.feature_browse.data.remote.response.AnimeResponseDTO
import retrofit2.http.GET

/**
 * What is the purpose of this service?
 * to access data from api
 * What does the @GET annotation do for us?
 * allows us to retrieve data from the api
 * Why are we using the suspend keyword?
 * function can be paused and resume when needed.
 */
interface JikanService {

    @GET("/v4/anime")
    suspend fun getAnimeList() : AnimeResponseDTO

}