package com.rave.jikan

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class JikanApplication : Application()