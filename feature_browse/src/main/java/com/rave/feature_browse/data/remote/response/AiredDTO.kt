package com.rave.feature_browse.data.remote.response

data class AiredDTO(
    val from: String,
    val prop: PropDTO,
    val string: String,
    val to: String?
)