package com.rave.feature_browse.di

import android.content.Context
import androidx.room.Room
import com.rave.feature_browse.data.local.JikanDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.migration.DisableInstallInCheck

/**
 * Set this up for Database Instances
 *
 * refer to [RemoteModule]
 */
@Module
@DisableInstallInCheck
object LocalModule {
    private val DATABASE_NAME = "jikan.db"

    @Volatile
    private var instance: JikanDatabase? = null

    @Provides
    fun getInstance(context: Context): JikanDatabase {
        return instance ?: synchronized(this) {
            instance ?: buildDatabase(context).also { instance = it }
        }
    }

    private fun buildDatabase(context: Context): JikanDatabase {
        return Room.databaseBuilder(context, JikanDatabase::class.java, DATABASE_NAME).build()
    }


}