package com.rave.feature_browse.di

import com.rave.feature_browse.data.remote.JikanService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dagger.hilt.migration.DisableInstallInCheck
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Singleton

/**
 * @Module lets use mark this object class for dagger to know this is where we will make some instances
 * @DisableInstallInCheck Since we cant use hilt but have to use dagger instead because its a feature module,
 *                       this annotation tells hilt compiler to ignore it's InstallIn Check
 */
@Module
@DisableInstallInCheck
object RemoteModule {

    @Provides
    fun providesRetrofit(): Retrofit = Retrofit.Builder()
        .baseUrl("https://api.jikan.moe")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    @Provides
    fun providesJikanService(retrofit: Retrofit): JikanService = retrofit.create()

}