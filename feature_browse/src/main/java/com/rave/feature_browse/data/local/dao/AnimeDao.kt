package com.rave.feature_browse.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.rave.feature_browse.data.local.entity.Anime

/**
 * Create and setup DAO that lets user
 * update new list of anime
 * get list of anime
 * get a single anime using Id
 */
@Dao
interface AnimeDao {
    @Query("SELECT * FROM anime")
    suspend fun getAnime(): List<Anime>

    @Insert
    suspend fun insert(animeList: List<Anime>)

    @Query("SELECT * FROM anime WHERE id = :id")
    suspend fun getAnimeById(id: Int): Anime
}