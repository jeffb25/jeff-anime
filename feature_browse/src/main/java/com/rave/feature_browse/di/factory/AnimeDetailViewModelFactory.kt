package com.rave.feature_browse.di.factory

import androidx.lifecycle.SavedStateHandle
import com.rave.feature_browse.presentation.viewmodel.AnimeDetailViewModel
import dagger.assisted.AssistedFactory

/*
@AssistedFactory
interface AnimeDetailViewModelFactory {

}*/
interface AnimeDetailViewModelFactory {
    fun create(handle: SavedStateHandle): AnimeDetailViewModel
}
