package com.rave.feature_browse.presentation.ui.browse

import com.rave.feature_browse.data.local.entity.Anime

data class AnimeListState(
    val isLoading: Boolean = false,
    val animeList: List<Anime> = emptyList(),
    val errorMsg: String? = null
)