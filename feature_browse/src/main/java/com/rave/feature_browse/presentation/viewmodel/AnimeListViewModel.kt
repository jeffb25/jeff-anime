package com.rave.feature_browse.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.rave.feature_browse.data.local.entity.Anime
import com.rave.feature_browse.domain.GetAnimeListUseCase
import com.rave.feature_browse.presentation.ui.browse.AnimeListState
import javax.inject.Inject

class AnimeListViewModel @Inject constructor(
    getAnimeListUseCase: GetAnimeListUseCase
) : ViewModel() {

    /**
     * What is "livdata { ... }" and what are we using it for here
     * wrapper that can be used with any data. mostly used in viewModel and pays attention to lifecycles
     * What does emit method do and what class is it a part off?
     * flow builder function creates a new flow where you can manually emit new values into the stream of data
     */
    val browseAnimeState = liveData {
        emit(AnimeListState(isLoading = true))
        val animeListResult: Result<List<Anime>> = getAnimeListUseCase()
        val state = AnimeListState(
            animeList = animeListResult.getOrNull() ?: emptyList(), // explain this line
            errorMsg = animeListResult.exceptionOrNull()?.message // explain this line
        )
        emit(state)
    }
}