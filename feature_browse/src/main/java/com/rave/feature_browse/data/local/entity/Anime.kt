package com.rave.feature_browse.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * What does the @Entity mean?
 * will have a liteSQL table in database
 * What does the @PrimaryKey mean?
 * marks field in SQL as primarykey.
 * What does the autoGenerate = true in the @PrimaryKey mean?
 * wil auto generate the primarykey to cover all rows in field
 * Why do we give id property a default value but not the other properties?
 * we use it so we can map or filter through the table. gives each item
 * its own id
 */
@Entity
data class Anime(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0 ,
    val title: String = "",
    val imageUrl: String = "" ,
    val synopsis: String = ""
)