package com.rave.jikan.di

import com.rave.jikan.annotations.DefaultDispatcher
import com.rave.jikan.annotations.IoDispatcher
import com.rave.jikan.annotations.MainDispatcher
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher

@EntryPoint
@InstallIn(SingletonComponent::class)
interface DispatcherDependencies {

    @DefaultDispatcher
    fun providesDefaultDispatcher(): CoroutineDispatcher

    @IoDispatcher
    fun providesIoDispatcher(): CoroutineDispatcher

    @MainDispatcher
    fun providesMainDispatcher(): CoroutineDispatcher
}
