package com.rave.feature_browse.data.remote.response

data class PropDTO(
    val from: FromDTO,
    val to: ToDTO
)