package com.rave.feature_browse.data.remote.response

data class ImagesDTO(
    val jpg: JpgDTO,
    val webp: WebpDTO
)