package com.rave.feature_browse.presentation.ui.browse_detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.rave.feature_browse.databinding.FragmentAnimeDetailBinding
import com.rave.feature_browse.di.DaggerBrowseComponent
import com.rave.feature_browse.presentation.viewmodel.AnimeDetailViewModel
import com.rave.jikan.di.DispatcherDependencies
import com.rave.jikan.ext.assistedViewModel
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.EntryPointAccessors
import javax.inject.Inject

class AnimeDetailFragment : Fragment() {

    private var _binding: FragmentAnimeDetailBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<AnimeDetailFragmentArgs>()

    @Inject
    lateinit var viewModelFactory: AnimeDetailViewModel.Factory

    private val animeDetailViewModel by assistedViewModel { viewModelFactory.create(it) }

    override fun onCreate(savedInstanceState: Bundle?) {
        initDagger()
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentAnimeDetailBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        animeDetailViewModel.browseAnimeState.observe(viewLifecycleOwner){ state ->
            binding.tvAnimeTitle.text = state.animeList.title
            binding.tvAnimeSynopsis.text = state.animeList.synopsis

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initDagger() {
        val appDependencies = EntryPointAccessors.fromApplication(
            requireContext().applicationContext, DispatcherDependencies::class.java
        )
        DaggerBrowseComponent.builder()
            .context(requireContext())
            .dispatcherDependencies(appDependencies)
            .build()
            .inject(this)
    }
}