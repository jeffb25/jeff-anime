package com.rave.feature_browse.data.remote.response

import com.google.gson.annotations.SerializedName

data class TrailerImageDTO(
    @SerializedName("image_url")
    val imageUrl: String?,
    @SerializedName("large_image_url")
    val largeImageUrl: String?,
    @SerializedName("maximum_image_url")
    val maximumImageUrl: String?,
    @SerializedName("medium_image_url")
    val mediumImageUrl: String?,
    @SerializedName("small_image_url")
    val smallImageUrl: String?,

)